const gulp = require('gulp');
const browserSync = require('browser-sync');
const sass = require('gulp-sass');
const autoprefixer = require('gulp-autoprefixer');
const cleanCss = require('gulp-clean-css');
const uglify = require('gulp-minify');
const del = require('del');
const concat = require('gulp-concat');

gulp.task('scripts', () => {
    gulp.src(['src/js/main.js'])
        .pipe(uglify({
            ext: {
                min: '.min.js'
            }
        }))
        .pipe(gulp.dest('src/js'))
        .pipe(browserSync.reload({ stream: true }))
});

gulp.task('sass', () => {
    return gulp.src('src/sass/**/*.sass')
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer({
            browsers: ['last 12 versions'],
            cascade: false
        }))
        .pipe(cleanCss({ compatibility: 'ie8' }))
        .pipe(concat('main.min.css'))
        .pipe(gulp.dest('src/css'))
        .pipe(browserSync.reload({ stream: true }));
});

gulp.task('browser-sync', () => {
    browserSync({
        server: {
            baseDir: 'src'
        },
        notify: false
    });
});

gulp.task('deleteBuild', () => {
	return del.sync('del');
});

gulp.task('watch', ['sass', 'scripts', 'browser-sync'], () => {
    gulp.watch('src/sass/**/*.sass', ['sass']);
    gulp.watch(['src/libs/**/*.js', 'src/js/main.js'], ['scripts']);
    gulp.watch('src/*.html', browserSync.reload);

});

gulp.task('build', ['deleteBuild', 'sass', 'scripts'], () => {

    let buildHtml = gulp.src([
        'src/*.html'
    ]).pipe(gulp.dest('build'));

    let buildCss = gulp.src([
        'src/css/main.min.css'
    ]).pipe(gulp.dest('build/css'));

    let buildJs = gulp.src([
        'src/js/main.min.js'
    ]).pipe(gulp.dest('build/js'));

    let buildFonts = gulp.src([
        'src/fonts/**/*']
    ).pipe(gulp.dest('build/fonts'));

});

gulp.task('default', ['watch']);
